# NeoPixel API for LabVIEW

API and demo for accessing [Adafruit's NeoPixel LEDs](https://www.adafruit.com/category/168) via [Arduino controllers](https://www.arduino.cc) from [NI LabVIEW(tm)](https://www.ni.com/en-us/shop/labview.html).

## :pencil: Documentation

All available documentation can be found in the project wiki at https://gitlab.com/WUELUG/neopixel-api-for-labview/-/wikis/

## :rocket: Installation

### :building_construction: Hardware

Even though the code should be executable on various Arduino controllers (and has been tested on some!), our harware of choice is as follows:

-  Arduino MKR Zero + Arduino MKR ETH Shield

### :wrench: LabVIEW 2014

The LabVIEW VIs are maintained in LabVIEW 2014.

### :link: Dependencies

For LabVIEW: None at this point in time.

For Arduino: See the #include defitions in the .ino files:

- SPI.h
- Ethernet.h
- EthernetUdp.h
- FastLED.h



## :bulb: Usage

Run the UDP Test sketch on your Arduino and the UDP Test VI on your PC. Make sure to update IP addresses according to your local setup.

## :construction: Maintenance 

This repository is maintained by [HAMPEL SOFTWARE ENGINEERING](https://www.hampel-soft.com) on behalf of the [Wuerzburg LabVIEW User Group](http://bit.ly/WUELUG). 


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


##  :beers: Credits

* Julian Lange (Siemens Energy)
* Bence Bartho (Hampel Software Engineering)
* Joerg Hampel (Hampel Software Engineering)


## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details.
