void rainbowwheel()
{
  unsigned long t0;
  unsigned long t1;
  unsigned long t2;
  unsigned long t3;
  for(uint16_t k = 0; k < NUM_LEDS; k++)
  {
    Serial.println(k);
    for(uint16_t i = 0; i < NUM_LEDS; i++) 
    {
      t0 = micros();
      m = (k+i)%(NUM_LEDS-1);
      hue = map(i, 0, (NUM_LEDS-1), 0, 255);
      hsv.h = hue;
      if (i == 0){
        hsv.h = 100;
      }
      else{
      }
      hsv2rgb_rainbow( hsv, rgb);
      leds[m] = rgb; 
    }
    t1 = micros();
    FastLED.show();
    t2 = micros();
    //FastLED.delay(5);
    t3 = micros();
    //Serial.println(t1-t0);
    //Serial.println(t2-t1);
    //Serial.println(t3-t2);
    //Serial.println();
  }
}
