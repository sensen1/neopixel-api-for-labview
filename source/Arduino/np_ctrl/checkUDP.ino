// ================================================================================
// Check if a UDP packet was sent, and if so, parse it and update the RGB variables
// ================================================================================
void checkUDP() {
  // The hue set via UDP 
  String udpString = "";
  int packetSize = Udp.parsePacket();
  if(packetSize)
  {
    // read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    udpString = packetBuffer;
    r = udpString.substring(0,3).toInt();
    g = udpString.substring(4,7).toInt();
    b = udpString.substring(8,11).toInt();
    Serial.print(r);
    Serial.print("/");
    Serial.print(g);
    Serial.print("/");
    Serial.println(b);

    /*
    Serial.print(packetBuffer);

    //Serial.print("Received packet of size ");
    //Serial.println(packetSize);
    Serial.print(" (from ");
    IPAddress remote = Udp.remoteIP();
    for (int i =0; i < 4; i++)
    {
      Serial.print(remote[i], DEC);
      if (i < 3)
      {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.print(Udp.remotePort());
    Serial.println(")");
    */
  }
}
// ================================================================================
