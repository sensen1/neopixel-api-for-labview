void progress()
{
  uint16_t progr_in = 0;
  uint16_t progr_map1 = 0;
  uint16_t progr_map2 = 0;
  uint16_t i = 0;

  for (progr_in = 0; progr_in < 100; progr_in++){
    progr_map1 = map(progr_in, 0, 99, Begin1, End1);
    progr_map2 = map(progr_in, 0, 99, End2, Begin2);
    for (i = 0; i < Begin1; i++){
      leds[i] = CRGB::Black; 
    }
    for (i = Begin1; i < Begin1 + progr_map1; i++){
      leds[i] = rgb; 
    }
    for (i = Begin1 + progr_map1; i < Begin2 - progr_map2; i++){
      leds[i] = CRGB::Black; 
    }
    for (i = Begin2 - progr_map2; i < Begin2; i++){
      leds[i] = rgb; 
    }
    for (i = Begin2; i < NUM_LEDS; i++){
      leds[i] = CRGB::Black; 
    }
    FastLED.show();
    FastLED.delay(500);
  }
}
