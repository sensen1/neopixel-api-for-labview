 void knightrider()
 {
  // ********************************************************************************
  // First slide the led in one direction
  for(int i = 0; i < NUM_LEDS; i++) {
    // Check if a new color value was sent via ethernet
    checkUDP();
    // Set the i'th led to the current color
    leds[i] = CRGB(r, g, b);
    // Show the leds
    FastLED.show(); 
    // now that we've shown the leds, reset the i'th led to black
    //leds[i] = CRGB::Black;
    fadeall();
    // Wait a little bit before we loop around and do it again
    delay(10);
  }

  // Now go in the other direction.  
  for(int i = (NUM_LEDS)-1; i >= 0; i--) {
    // Check if a new color value was sent via ethernet
    checkUDP();
    // Set the i'th led to the current color
    leds[i] = CRGB(r, g, b);
    // Show the leds
    FastLED.show();
    // now that we've shown the leds, reset the i'th led to black
    //leds[i] = CRGB::Black;
    fadeall();
    // Wait a little bit before we loop around and do it again
    delay(10);
  }
  // ********************************************************************************
}



// ********************************************************************************
// scale down a RGB to N 256ths of it's current brightness, using 'plain math' dimming 
// rules, which means that if the low light levels may dim all the way to 100% black.
// ********************************************************************************
void fadeall() {
  for(int i = 0; i < NUM_LEDS; i++) { 
    // scale down to 250/256ths
    leds[i].nscale8(250); 
  } 
}
